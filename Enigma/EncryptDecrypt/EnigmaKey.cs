﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enigma.EncryptDecrypt
{
    [Serializable]
    public class EnigmaKey
    {
        private List<char> _key;
        private string _password;
        private int _shift;

        public EnigmaKey(List<char> key, string password, int shift)
        {
            _key = key;
            _password = password;
            _shift = shift;
        }
        
        public List<char> GetKey(string password)
        {
            if (password == _password)
                return new List<char>(_key);
            else
                return new List<char>();
        }

        public int GetShift(string password)
        {
            if (password == _password)
                return _shift;
            else
                return 0;
        }
        
    }
}
