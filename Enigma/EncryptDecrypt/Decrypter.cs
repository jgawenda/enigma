﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enigma.EncryptDecrypt
{
    public class Decrypter
    {
        private List<char> _chars;

        private string DecryptAPhrase(string phrase, int shift)
        {
            string decryptedPhrase = "";
            int index = 0;

            foreach (char character in phrase)
            {
                if (_chars.Contains(character))
                {
                    index = _chars.IndexOf(character) - shift;

                    if (index < 0)
                        index = index + _chars.Count;

                    decryptedPhrase += _chars.ElementAt(index);

                }
                else if (character.ToString() == "\n")
                    decryptedPhrase += "\n";
                else
                    decryptedPhrase += '?';
            }

            return decryptedPhrase;
        }

        public string Decrypt(string phrase, List<char> key, int shift)
        {
            _chars = key;

            return DecryptAPhrase(phrase, shift);
        }

    }
}
