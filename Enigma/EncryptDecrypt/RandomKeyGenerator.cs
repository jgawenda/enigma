﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enigma.EncryptDecrypt
{
    public class RandomKeyGenerator
    {
        private List<char> _allAvailableChars;

        private void FillAvailableChars()
        {
            _allAvailableChars = new List<char>() { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'W', 'X', 'Y', 'Z', ' ', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '!', '?', '"', '{', '}', '\'', '\\', '/', '<', '>', '[', ']', '|', '-', '_', ',', '.', '+', '=', ':', '#', '$', '%', '^', '&', '*', '(', ')', 'ą', 'ć', 'ę', 'ł', 'ń', 'ś', 'ó', 'ź', 'ż', 'Ą', 'Ć', 'Ę', 'Ł', 'Ń', 'Ś', 'Ó', 'Ź', 'Ż', '`', '~', '¡', '¢', '£', '¤', '¥', '§', '¨', '©', 'ª', '«', '¬', '®', '¯', '°', '±', '²', '³', '´', 'µ', '¶', '·', '¸', '¹', 'º', '»', '¼', '½', '¾', '¿', 'Ԁ', 'ԁ', 'Ԃ', 'ԃ', 'Ԅ', 'ԅ', 'Ԇ', 'ԇ', 'Ԉ', 'ԉ', 'Ԋ', 'ԋ', 'Ԍ', 'ԍ', 'Ԏ', 'ԏ', 'Ԑ', 'ԑ', 'Ԓ', 'ԓ', 'Ԛ', 'ԛ', 'Ա', 'Բ', 'Գ', 'Դ', 'Ե', 'Զ', 'Է', 'Ը', 'Թ', 'Ժ', 'Ի', 'Լ', 'Խ', 'Ծ', 'Կ', 'Հ', 'Ձ', 'Ղ', 'Ճ', 'Մ', 'Յ', 'Ն', 'Շ', 'Ո', 'Չ', 'Պ', 'Ջ', 'Ռ', 'Ս', 'Վ', 'Տ', 'ä', 'ö', 'ü', 'ß', 'Ä', 'Ö', 'Ü' };
        }

        private EnigmaKey GenerateNewRandomKey(string password)
        {
            Random random = new Random();

            FillAvailableChars();

            List<char> outputKey = new List<char>();
            int index = 0;

            while (_allAvailableChars.Count > 0)
            {
                index = random.Next(0, _allAvailableChars.Count - 1);
                outputKey.Add(_allAvailableChars.ElementAt(index));
                _allAvailableChars.RemoveAt(index);
            }

            Random randomShift = new Random();

            return new EnigmaKey(outputKey, password, randomShift.Next(3, 8));
        }

        public EnigmaKey Generate(string password)
        {
            return GenerateNewRandomKey(password);
        }

    }
}
