﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enigma.EncryptDecrypt
{
    public class Encrypter
    {
        private List<char> _chars;

        private string EncryptAPhrase(string phrase, int shift)
        {
            string encryptedPhrase = "";
            int newIndex = 0;
            
            foreach (char character in phrase)
            {
                if (_chars.Contains(character))
                {
                    newIndex = _chars.IndexOf(character) + shift;

                    if (newIndex >= _chars.Count)
                        newIndex = newIndex - _chars.Count;

                    encryptedPhrase += _chars.ElementAt(newIndex);
                }
                else if (character.ToString() == "\n")
                    encryptedPhrase += "\n";
                else
                    encryptedPhrase += '?';
            }

            return encryptedPhrase;
        }

        public string Encrypt(string phrase, List<char> key, int shift)
        {
            _chars = key;

            return EncryptAPhrase(phrase, shift);
        }

    }
}
