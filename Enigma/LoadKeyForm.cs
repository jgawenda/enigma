﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Enigma.Actions;
using Enigma.EncryptDecrypt;

namespace Enigma
{
    public partial class LoadKeyForm : Form
    {
        private Validator _validator;
        private SerializeOperator _serializeOperator;
        private EnigmaComponents _enigmaComponents;
        private OpenFileDialog _openFileDialog;
        private Label _enigmaKeyLabel;
        private TabStopOperator _tabStopOperator;

        public LoadKeyForm(EnigmaComponents enigmaComponents, OpenFileDialog openFileDialog, Label enigmaKeyLabel)
        {
            InitializeComponent();

            _validator = new Validator(new List<TextBox>() { textBoxPIN });
            _serializeOperator = new SerializeOperator();
            _enigmaComponents = enigmaComponents;
            _openFileDialog = openFileDialog;
            _enigmaKeyLabel = enigmaKeyLabel;
            _tabStopOperator = new TabStopOperator(new List<Control>() { labelInfo, labelKey, labelKeyName });
            _tabStopOperator.Disable();

            labelKeyName.Text = _openFileDialog.SafeFileName;
            DialogResult = DialogResult.Abort;
        }

        private void ButtonOK_Click(object sender, EventArgs e)
        {
            if (_validator.ValidateBoxes(Validator.ValidateType.PinNumber))
            {
                if (_serializeOperator.LoadEnigmaKey(_openFileDialog.FileName, _openFileDialog.SafeFileName, textBoxPIN.Text, _enigmaComponents, _enigmaKeyLabel))
                {
                    DialogResult = DialogResult.OK;
                    this.Close();
                }
            }
        }

    }
}
