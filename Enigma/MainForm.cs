﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Enigma.EncryptDecrypt;
using Enigma.Actions;

namespace Enigma
{
    public partial class MainForm : Form
    {
        private EnigmaComponents _enigmaComponents;
        private SerializeOperator _serializeOperator;
        private FormButtonsOperator _formButtonsOperator;
        private TabStopOperator _tabStopOperator;

        public MainForm()
        {
            InitializeComponent();

            _enigmaComponents = new EnigmaComponents();
            _serializeOperator = new SerializeOperator();
            _formButtonsOperator = new FormButtonsOperator(new List<Button>() { buttonEncrypt, buttonDecrypt });
            _tabStopOperator = new TabStopOperator(new List<Control>() { labelLoadedKey, labelLoadedKeyName });
            _tabStopOperator.Disable();
        }

        private void ButtonLoadKey_Click(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                LoadKeyForm loadKeyForm = new LoadKeyForm(_enigmaComponents, openFileDialog, labelLoadedKeyName);
                
                this.Visible = false;
                var result = loadKeyForm.ShowDialog();
                this.Visible = true;
                
                switch (result)
                {
                    case DialogResult.OK:
                        _formButtonsOperator.EnableAllButtons();
                        break;
                    case DialogResult.Abort:
                        _formButtonsOperator.DisableAllButtons();
                        break;
                }
                
            }

        }

        private void GenerateNewKeyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            NewKeyForm newKeyForm = new NewKeyForm();
            newKeyForm.ShowDialog();
        }

        private void ButtonEncrypt_Click(object sender, EventArgs e)
        {
            EncryptForm encryptForm = new EncryptForm(labelLoadedKeyName.Text, _enigmaComponents);
            encryptForm.ShowDialog();
        }

        private void ButtonDecrypt_Click(object sender, EventArgs e)
        {
            DecryptForm decryptForm = new DecryptForm(labelLoadedKeyName.Text, _enigmaComponents);
            decryptForm.ShowDialog();
        }
    }
}
