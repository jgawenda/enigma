﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Enigma.Actions;
using Enigma.EncryptDecrypt;

namespace Enigma
{
    public partial class EncryptForm : Form
    {
        private EncryptOperator _encryptOperator;
        private TabStopOperator _tabStopOperator;

        public EncryptForm(string keyFileName, EnigmaComponents enigmaComponents)
        {
            InitializeComponent();
            _encryptOperator = new EncryptOperator(enigmaComponents, richTextBoxBefore, richTextBoxAfter);
            _tabStopOperator = new TabStopOperator(new List<Control>() { labelEncryptedText, labelKeyPassword, labelLoadedKey, labelLoadedKeyName, labelNormalText });
            _tabStopOperator.Disable();

            labelLoadedKeyName.Text = keyFileName;
        }

        private void ButtonEncrypt_Click(object sender, EventArgs e)
        {
            _encryptOperator.Encrypt(textBoxPassword.Text);
        }
    }
}
