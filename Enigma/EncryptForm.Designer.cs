﻿namespace Enigma
{
    partial class EncryptForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EncryptForm));
            this.richTextBoxBefore = new System.Windows.Forms.RichTextBox();
            this.richTextBoxAfter = new System.Windows.Forms.RichTextBox();
            this.labelLoadedKey = new System.Windows.Forms.Label();
            this.labelLoadedKeyName = new System.Windows.Forms.Label();
            this.buttonEncrypt = new System.Windows.Forms.Button();
            this.textBoxPassword = new System.Windows.Forms.TextBox();
            this.labelKeyPassword = new System.Windows.Forms.Label();
            this.labelNormalText = new System.Windows.Forms.Label();
            this.labelEncryptedText = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // richTextBoxBefore
            // 
            this.richTextBoxBefore.Location = new System.Drawing.Point(13, 102);
            this.richTextBoxBefore.Name = "richTextBoxBefore";
            this.richTextBoxBefore.Size = new System.Drawing.Size(287, 242);
            this.richTextBoxBefore.TabIndex = 1;
            this.richTextBoxBefore.Text = "";
            // 
            // richTextBoxAfter
            // 
            this.richTextBoxAfter.Location = new System.Drawing.Point(387, 102);
            this.richTextBoxAfter.Name = "richTextBoxAfter";
            this.richTextBoxAfter.Size = new System.Drawing.Size(287, 242);
            this.richTextBoxAfter.TabIndex = 3;
            this.richTextBoxAfter.Text = "";
            // 
            // labelLoadedKey
            // 
            this.labelLoadedKey.AutoSize = true;
            this.labelLoadedKey.Location = new System.Drawing.Point(12, 9);
            this.labelLoadedKey.Name = "labelLoadedKey";
            this.labelLoadedKey.Size = new System.Drawing.Size(67, 13);
            this.labelLoadedKey.TabIndex = 2;
            this.labelLoadedKey.Text = "Loaded Key:";
            // 
            // labelLoadedKeyName
            // 
            this.labelLoadedKeyName.AutoSize = true;
            this.labelLoadedKeyName.Location = new System.Drawing.Point(85, 9);
            this.labelLoadedKeyName.Name = "labelLoadedKeyName";
            this.labelLoadedKeyName.Size = new System.Drawing.Size(31, 13);
            this.labelLoadedKeyName.TabIndex = 3;
            this.labelLoadedKeyName.Text = "none";
            // 
            // buttonEncrypt
            // 
            this.buttonEncrypt.Location = new System.Drawing.Point(306, 206);
            this.buttonEncrypt.Name = "buttonEncrypt";
            this.buttonEncrypt.Size = new System.Drawing.Size(75, 23);
            this.buttonEncrypt.TabIndex = 2;
            this.buttonEncrypt.Text = "Encrypt";
            this.buttonEncrypt.UseVisualStyleBackColor = true;
            this.buttonEncrypt.Click += new System.EventHandler(this.ButtonEncrypt_Click);
            // 
            // textBoxPassword
            // 
            this.textBoxPassword.Location = new System.Drawing.Point(94, 36);
            this.textBoxPassword.Name = "textBoxPassword";
            this.textBoxPassword.PasswordChar = '*';
            this.textBoxPassword.Size = new System.Drawing.Size(100, 20);
            this.textBoxPassword.TabIndex = 0;
            // 
            // labelKeyPassword
            // 
            this.labelKeyPassword.AutoSize = true;
            this.labelKeyPassword.Location = new System.Drawing.Point(12, 39);
            this.labelKeyPassword.Name = "labelKeyPassword";
            this.labelKeyPassword.Size = new System.Drawing.Size(76, 13);
            this.labelKeyPassword.TabIndex = 6;
            this.labelKeyPassword.Text = "Key password:";
            // 
            // labelNormalText
            // 
            this.labelNormalText.AutoSize = true;
            this.labelNormalText.Location = new System.Drawing.Point(12, 86);
            this.labelNormalText.Name = "labelNormalText";
            this.labelNormalText.Size = new System.Drawing.Size(63, 13);
            this.labelNormalText.TabIndex = 7;
            this.labelNormalText.Text = "Normal text:";
            // 
            // labelEncryptedText
            // 
            this.labelEncryptedText.AutoSize = true;
            this.labelEncryptedText.Location = new System.Drawing.Point(384, 86);
            this.labelEncryptedText.Name = "labelEncryptedText";
            this.labelEncryptedText.Size = new System.Drawing.Size(78, 13);
            this.labelEncryptedText.TabIndex = 8;
            this.labelEncryptedText.Text = "Encrypted text:";
            // 
            // EncryptForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(686, 358);
            this.Controls.Add(this.labelEncryptedText);
            this.Controls.Add(this.labelNormalText);
            this.Controls.Add(this.labelKeyPassword);
            this.Controls.Add(this.textBoxPassword);
            this.Controls.Add(this.buttonEncrypt);
            this.Controls.Add(this.labelLoadedKeyName);
            this.Controls.Add(this.labelLoadedKey);
            this.Controls.Add(this.richTextBoxAfter);
            this.Controls.Add(this.richTextBoxBefore);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "EncryptForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Encrypt message";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox richTextBoxBefore;
        private System.Windows.Forms.RichTextBox richTextBoxAfter;
        private System.Windows.Forms.Label labelLoadedKey;
        private System.Windows.Forms.Label labelLoadedKeyName;
        private System.Windows.Forms.Button buttonEncrypt;
        private System.Windows.Forms.TextBox textBoxPassword;
        private System.Windows.Forms.Label labelKeyPassword;
        private System.Windows.Forms.Label labelNormalText;
        private System.Windows.Forms.Label labelEncryptedText;
    }
}