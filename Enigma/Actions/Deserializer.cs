﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Forms;

namespace Enigma.Actions
{
    class Deserializer
    {
        public object Deserialize(string path)
        {
            object deserialized;

            using (Stream stream = File.Open(path, FileMode.Open))
            {
                BinaryFormatter binaryFormatter = new BinaryFormatter();
                
                try
                {
                    deserialized = binaryFormatter.Deserialize(stream);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error occured", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    deserialized = new object();
                }
                finally
                {
                    stream.Close();
                }

            }

            return deserialized;
        }

    }
}
