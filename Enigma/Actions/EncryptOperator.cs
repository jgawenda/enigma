﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Enigma.EncryptDecrypt;

namespace Enigma.Actions
{
    public class EncryptOperator
    {
        private Encrypter _encrypter;
        private EnigmaComponents _enigmaComponents;
        private RichTextBox _richTextBefore;
        private RichTextBox _richTextAfter;

        public EncryptOperator(EnigmaComponents enigmaComponents, RichTextBox boxBefore, RichTextBox boxAfter)
        {
            _encrypter = new Encrypter();
            _enigmaComponents = enigmaComponents;
            _richTextBefore = boxBefore;
            _richTextAfter = boxAfter;
        }

        public void Encrypt(string password)
        {
            _richTextAfter.Text = _encrypter.Encrypt(_richTextBefore.Text, _enigmaComponents.EnigmaKey.GetKey(password), _enigmaComponents.EnigmaKey.GetShift(password));
        }

    }
}
