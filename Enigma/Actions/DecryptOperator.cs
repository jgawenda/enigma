﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Enigma.EncryptDecrypt;

namespace Enigma.Actions
{
    class DecryptOperator
    {
        private Decrypter _decrypter;
        private EnigmaComponents _enigmaComponents;
        private RichTextBox _richTextBefore;
        private RichTextBox _richTextAfter;

        public DecryptOperator(EnigmaComponents enigmaComponents, RichTextBox boxBefore, RichTextBox boxAfter)
        {
            _decrypter = new Decrypter();
            _enigmaComponents = enigmaComponents;
            _richTextBefore = boxBefore;
            _richTextAfter = boxAfter;
        }

        public void Decrypt(string password)
        {
            _richTextAfter.Text = _decrypter.Decrypt(_richTextBefore.Text, _enigmaComponents.EnigmaKey.GetKey(password), _enigmaComponents.EnigmaKey.GetShift(password));
        }

    }
}
