﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Forms;
using System.Security.Cryptography;
using Enigma.EncryptDecrypt;

namespace Enigma.Actions
{
    public class CryptoSerializer
    {
        private DESCryptoServiceProvider _des = new DESCryptoServiceProvider();

        public bool Serialize(string path, byte[] pinNumber, EnigmaKey enigmaKey)
        {
            bool result = true;

            using (FileStream fileStream = new FileStream(path, FileMode.Create, FileAccess.Write))
            {
                using (CryptoStream cryptoStream = new CryptoStream(fileStream, _des.CreateEncryptor(pinNumber, pinNumber), CryptoStreamMode.Write))
                {
                    BinaryFormatter binaryFormatter = new BinaryFormatter();

                    try
                    {
                        binaryFormatter.Serialize(cryptoStream, enigmaKey);
                        MessageBox.Show(String.Format("Key {0} created succesfully", path), "New key created", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        result = true;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error occured", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        result = false;
                    }

                }
            }

            return result;
        }

    }
}
