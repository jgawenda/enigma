﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace Enigma.Actions
{
    class ValidatorNewKeyForm : Validator
    {
        private TextBox _textBoxPin;

        public ValidatorNewKeyForm(List<TextBox> textBoxes, TextBox textBoxPin) : base (textBoxes)
        {
            _textBoxPin = textBoxPin;
        }
        
        public bool ValidateAllBoxes()
        {
            bool resultTextBoxes = base.ValidateBoxes(ValidateType.BlankBox);
            bool resultTextBoxPin = base.ValidateBoxes(ValidateType.PinNumber, new List<TextBox>() { _textBoxPin });

            if (resultTextBoxes && resultTextBoxPin)
                return true;
            else
                return false;

        }

    }
}
