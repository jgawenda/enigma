﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using Enigma.EncryptDecrypt;

namespace Enigma.Actions
{
    class SerializeOperator
    {
        private CryptoSerializer _cryptoSerializer = new CryptoSerializer();
        private CryptoDeserializer _cryptoDeserializer = new CryptoDeserializer();

        private string GetShortKeyFilename(string fileName)
        {
            if (fileName.Length > 15)
                return String.Format("{0}...", fileName.Substring(0, 15));
            else
                return fileName;
        }

        private byte[] ConvertToBytePin(string valueToConvert)
        {
            byte[] outputPin = new byte[valueToConvert.Length];

            for (int i=0; i<valueToConvert.Length; i++)
            {
                outputPin[i] = Convert.ToByte(valueToConvert[i]);
            }

            return outputPin;
        }
        
        public bool LoadEnigmaKey(string filePath, string fileName, string pinNumber, EnigmaComponents enigmaComponents, Label enigmaKeyLabel)
        {
            object deserialized = _cryptoDeserializer.Deserialize(filePath, ConvertToBytePin(pinNumber));
            
            if (deserialized is EnigmaKey)
            {
                enigmaComponents.EnigmaKey = (EnigmaKey)deserialized;
                enigmaKeyLabel.Text = GetShortKeyFilename(fileName);
                enigmaKeyLabel.ForeColor = Color.Black;
                return true;
            }
            else
            {
                enigmaKeyLabel.Text = "none";
                enigmaKeyLabel.ForeColor = Color.OrangeRed;
                return false;
            }

        }

        public bool SaveNewEnigmaKey(string fileName, string pinNumber, EnigmaKey enigmaKey)
        {
            return _cryptoSerializer.Serialize(String.Format("{0}.ekey", fileName), ConvertToBytePin(pinNumber), enigmaKey);
        }

    }
}
