﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Forms;
using Enigma.EncryptDecrypt;

namespace Enigma.Actions
{
    public class Serializer
    {
        public bool Serialize(string path, EnigmaKey enigmaKey)
        {
            bool result = true;

            using (Stream stream = File.Open(path, FileMode.Create))
            {
                BinaryFormatter binaryFormatter = new BinaryFormatter();

                try
                {
                    binaryFormatter.Serialize(stream, enigmaKey);
                    MessageBox.Show(String.Format("Key {0} created succesfully", path), "New key created", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    result = true;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error occured", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    result = false;
                }

                stream.Close();
            }

            return result;
        }

    }
}
