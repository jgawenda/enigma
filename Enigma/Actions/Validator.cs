﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace Enigma.Actions
{
    public class Validator
    {
        /*
         * type of validation
         * BlankBox - check TextBoxes if they are not blank
         * PinNumber - check TextBoxes if they are 8 char long, are convertible to int and not blank
        */
        public enum ValidateType
        {
            BlankBox,
            PinNumber
        }

        private List<TextBox> _textBoxes;

        public Validator(List<TextBox> textBoxes)
        {
            _textBoxes = textBoxes;
        }
        
        private bool ValidateTextBoxesBlank(List<TextBox> textBoxes)
        {
            bool result = true;

            foreach (TextBox textBox in textBoxes)
            {
                if (textBox.Text == "")
                {
                    textBox.BackColor = Color.OrangeRed;
                    result = false;
                }
                else
                    textBox.BackColor = Color.White;
            }

            return result;
        }

        private bool ValidateTextBoxesPIN(List<TextBox> textBoxes)
        {
            bool result = true;
            int parseResult = 0;

            foreach (TextBox textBox in textBoxes)
            {
                if (textBox.Text == "" || !Int32.TryParse(textBox.Text, out parseResult) || textBox.Text.Length != 8)
                {
                    textBox.BackColor = Color.OrangeRed;
                    result = false;
                }
            }

            return result;
        }

        private bool ProceedValidation(ValidateType validateType, List<TextBox> textBoxes)
        {
            bool result = false;
            
            switch (validateType)
            {
                case ValidateType.BlankBox:
                    result = ValidateTextBoxesBlank(textBoxes);
                    break;
                case ValidateType.PinNumber:
                    result = ValidateTextBoxesPIN(textBoxes);
                    break;
            }

            return result;
        }

        public bool ValidateBoxes(ValidateType validateType, List<TextBox> textBoxes)
        {
            return ProceedValidation(validateType, textBoxes);
        }
        
        public bool ValidateBoxes(ValidateType validateType)
        {
            return ProceedValidation(validateType, this._textBoxes);
        }

    }
}
