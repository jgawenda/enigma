﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Enigma.Actions
{
    class TabStopOperator
    {
        private List<Control> _controls;

        public TabStopOperator(List<Control> controls)
        {
            _controls = controls;
        }

        public void Disable()
        {
            foreach (Control element in _controls)
            {
                element.TabStop = false;
            }
        }

        public void Enable()
        {
            foreach(Control element in _controls)
            {
                element.TabStop = true;
            }
        }

    }
}
