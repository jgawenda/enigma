﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Forms;
using System.Security.Cryptography;
using Enigma.EncryptDecrypt;

namespace Enigma.Actions
{
    class CryptoDeserializer
    {
        private DESCryptoServiceProvider _des = new DESCryptoServiceProvider();
        
        public object Deserialize(string path, byte[] pinNumber)
        {
            object deserialized;

            using (FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read))
            {
                try
                {
                    using (CryptoStream cryptoStream = new CryptoStream(fs, _des.CreateDecryptor(pinNumber, pinNumber), CryptoStreamMode.Read))
                    {
                        BinaryFormatter binaryFormatter = new BinaryFormatter();

                        try
                        {
                            deserialized = binaryFormatter.Deserialize(cryptoStream);
                        }
                        catch (SerializationException ex)
                        {
                            MessageBox.Show("Error occured during Key opening. Are you sure the PIN number is correct?", "Error occured", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            deserialized = new object();
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message, "Error occured", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            deserialized = new object();
                        }

                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Couldn't open Key. PIN number is incorrect.", "Error occured", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    deserialized = new object();
                }
                
            }

            return deserialized;
        }

    }
}
