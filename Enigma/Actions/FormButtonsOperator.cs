﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Enigma.Actions
{
    class FormButtonsOperator
    {
        private List<Button> _buttons;

        public FormButtonsOperator(List<Button> buttons)
        {
            _buttons = buttons;
        }

        public void DisableAllButtons()
        {
            foreach (Button button in _buttons)
            {
                button.Enabled = false;
            }
        }

        public void EnableAllButtons()
        {
            foreach (Button button in _buttons)
            {
                button.Enabled = true;
            }
        }

    }
}
