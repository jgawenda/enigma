﻿namespace Enigma
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.buttonEncrypt = new System.Windows.Forms.Button();
            this.buttonDecrypt = new System.Windows.Forms.Button();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.labelLoadedKey = new System.Windows.Forms.Label();
            this.labelLoadedKeyName = new System.Windows.Forms.Label();
            this.buttonLoadKey = new System.Windows.Forms.Button();
            this.toolStrip = new System.Windows.Forms.ToolStrip();
            this.toolStripDropDownButton1 = new System.Windows.Forms.ToolStripDropDownButton();
            this.generateNewKeyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonEncrypt
            // 
            this.buttonEncrypt.Enabled = false;
            this.buttonEncrypt.Location = new System.Drawing.Point(13, 91);
            this.buttonEncrypt.Name = "buttonEncrypt";
            this.buttonEncrypt.Size = new System.Drawing.Size(75, 23);
            this.buttonEncrypt.TabIndex = 1;
            this.buttonEncrypt.Text = "Encrypt";
            this.buttonEncrypt.UseVisualStyleBackColor = true;
            this.buttonEncrypt.Click += new System.EventHandler(this.ButtonEncrypt_Click);
            // 
            // buttonDecrypt
            // 
            this.buttonDecrypt.Enabled = false;
            this.buttonDecrypt.Location = new System.Drawing.Point(94, 91);
            this.buttonDecrypt.Name = "buttonDecrypt";
            this.buttonDecrypt.Size = new System.Drawing.Size(75, 23);
            this.buttonDecrypt.TabIndex = 2;
            this.buttonDecrypt.Text = "Decrypt";
            this.buttonDecrypt.UseVisualStyleBackColor = true;
            this.buttonDecrypt.Click += new System.EventHandler(this.ButtonDecrypt_Click);
            // 
            // openFileDialog
            // 
            this.openFileDialog.Filter = "Enigma key|*.ekey";
            // 
            // labelLoadedKey
            // 
            this.labelLoadedKey.AutoSize = true;
            this.labelLoadedKey.Location = new System.Drawing.Point(13, 34);
            this.labelLoadedKey.Name = "labelLoadedKey";
            this.labelLoadedKey.Size = new System.Drawing.Size(70, 13);
            this.labelLoadedKey.TabIndex = 7;
            this.labelLoadedKey.Text = "Loaded KEY:";
            // 
            // labelLoadedKeyName
            // 
            this.labelLoadedKeyName.AutoSize = true;
            this.labelLoadedKeyName.ForeColor = System.Drawing.Color.OrangeRed;
            this.labelLoadedKeyName.Location = new System.Drawing.Point(91, 34);
            this.labelLoadedKeyName.Name = "labelLoadedKeyName";
            this.labelLoadedKeyName.Size = new System.Drawing.Size(31, 13);
            this.labelLoadedKeyName.TabIndex = 3;
            this.labelLoadedKeyName.Text = "none";
            // 
            // buttonLoadKey
            // 
            this.buttonLoadKey.Location = new System.Drawing.Point(55, 59);
            this.buttonLoadKey.Name = "buttonLoadKey";
            this.buttonLoadKey.Size = new System.Drawing.Size(75, 23);
            this.buttonLoadKey.TabIndex = 0;
            this.buttonLoadKey.Text = "Load key";
            this.buttonLoadKey.UseVisualStyleBackColor = true;
            this.buttonLoadKey.Click += new System.EventHandler(this.ButtonLoadKey_Click);
            // 
            // toolStrip
            // 
            this.toolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripDropDownButton1});
            this.toolStrip.Location = new System.Drawing.Point(0, 0);
            this.toolStrip.Name = "toolStrip";
            this.toolStrip.Size = new System.Drawing.Size(182, 25);
            this.toolStrip.TabIndex = 5;
            this.toolStrip.Text = "toolStrip";
            // 
            // toolStripDropDownButton1
            // 
            this.toolStripDropDownButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripDropDownButton1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.generateNewKeyToolStripMenuItem});
            this.toolStripDropDownButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton1.Name = "toolStripDropDownButton1";
            this.toolStripDropDownButton1.Size = new System.Drawing.Size(49, 22);
            this.toolStripDropDownButton1.Text = "Tools";
            // 
            // generateNewKeyToolStripMenuItem
            // 
            this.generateNewKeyToolStripMenuItem.Name = "generateNewKeyToolStripMenuItem";
            this.generateNewKeyToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.generateNewKeyToolStripMenuItem.Text = "Generate new key";
            this.generateNewKeyToolStripMenuItem.Click += new System.EventHandler(this.GenerateNewKeyToolStripMenuItem_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(182, 128);
            this.Controls.Add(this.toolStrip);
            this.Controls.Add(this.buttonLoadKey);
            this.Controls.Add(this.labelLoadedKeyName);
            this.Controls.Add(this.labelLoadedKey);
            this.Controls.Add(this.buttonDecrypt);
            this.Controls.Add(this.buttonEncrypt);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Enigma";
            this.toolStrip.ResumeLayout(false);
            this.toolStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonEncrypt;
        private System.Windows.Forms.Button buttonDecrypt;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.Label labelLoadedKey;
        private System.Windows.Forms.Label labelLoadedKeyName;
        private System.Windows.Forms.Button buttonLoadKey;
        private System.Windows.Forms.ToolStrip toolStrip;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton1;
        private System.Windows.Forms.ToolStripMenuItem generateNewKeyToolStripMenuItem;
    }
}

