﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Enigma.Actions;
using Enigma.EncryptDecrypt;

namespace Enigma
{
    public partial class DecryptForm : Form
    {
        private DecryptOperator _decryptOperator;
        private TabStopOperator _tabStopOperator;

        public DecryptForm(string keyFileName, EnigmaComponents enigmaComponents)
        {
            InitializeComponent();
            _decryptOperator = new DecryptOperator(enigmaComponents, richTextBoxBefore, richTextBoxAfter);
            _tabStopOperator = new TabStopOperator(new List<Control>() { labelEncryptedText, labelKeyPassword, labelLoadedKey, labelLoadedKeyName, labelNormalText });
            _tabStopOperator.Disable();

            labelLoadedKeyName.Text = keyFileName;
        }

        private void ButtonDecrypt_Click(object sender, EventArgs e)
        {
            _decryptOperator.Decrypt(textBoxPassword.Text);
        }

    }
}
