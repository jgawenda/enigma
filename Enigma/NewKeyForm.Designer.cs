﻿namespace Enigma
{
    partial class NewKeyForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NewKeyForm));
            this.textBoxFileName = new System.Windows.Forms.TextBox();
            this.buttonGenerate = new System.Windows.Forms.Button();
            this.labelFileName = new System.Windows.Forms.Label();
            this.labelEkey = new System.Windows.Forms.Label();
            this.textBoxPassword = new System.Windows.Forms.TextBox();
            this.labelPassword = new System.Windows.Forms.Label();
            this.labelKeyPIN = new System.Windows.Forms.Label();
            this.textBoxKeyPIN = new System.Windows.Forms.TextBox();
            this.labelPinHelp = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // textBoxFileName
            // 
            this.textBoxFileName.Location = new System.Drawing.Point(70, 12);
            this.textBoxFileName.Name = "textBoxFileName";
            this.textBoxFileName.Size = new System.Drawing.Size(100, 20);
            this.textBoxFileName.TabIndex = 0;
            this.textBoxFileName.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // buttonGenerate
            // 
            this.buttonGenerate.Location = new System.Drawing.Point(75, 133);
            this.buttonGenerate.Name = "buttonGenerate";
            this.buttonGenerate.Size = new System.Drawing.Size(75, 23);
            this.buttonGenerate.TabIndex = 3;
            this.buttonGenerate.Text = "Generate";
            this.buttonGenerate.UseVisualStyleBackColor = true;
            this.buttonGenerate.Click += new System.EventHandler(this.ButtonGenerate_Click);
            // 
            // labelFileName
            // 
            this.labelFileName.AutoSize = true;
            this.labelFileName.Location = new System.Drawing.Point(12, 15);
            this.labelFileName.Name = "labelFileName";
            this.labelFileName.Size = new System.Drawing.Size(55, 13);
            this.labelFileName.TabIndex = 11;
            this.labelFileName.Text = "File name:";
            // 
            // labelEkey
            // 
            this.labelEkey.AutoSize = true;
            this.labelEkey.Location = new System.Drawing.Point(172, 15);
            this.labelEkey.Name = "labelEkey";
            this.labelEkey.Size = new System.Drawing.Size(33, 13);
            this.labelEkey.TabIndex = 4;
            this.labelEkey.Text = ".ekey";
            // 
            // textBoxPassword
            // 
            this.textBoxPassword.Location = new System.Drawing.Point(70, 54);
            this.textBoxPassword.Name = "textBoxPassword";
            this.textBoxPassword.PasswordChar = '*';
            this.textBoxPassword.Size = new System.Drawing.Size(100, 20);
            this.textBoxPassword.TabIndex = 1;
            // 
            // labelPassword
            // 
            this.labelPassword.AutoSize = true;
            this.labelPassword.Location = new System.Drawing.Point(12, 57);
            this.labelPassword.Name = "labelPassword";
            this.labelPassword.Size = new System.Drawing.Size(56, 13);
            this.labelPassword.TabIndex = 6;
            this.labelPassword.Text = "Password:";
            // 
            // labelKeyPIN
            // 
            this.labelKeyPIN.AutoSize = true;
            this.labelKeyPIN.Location = new System.Drawing.Point(12, 83);
            this.labelKeyPIN.Name = "labelKeyPIN";
            this.labelKeyPIN.Size = new System.Drawing.Size(76, 13);
            this.labelKeyPIN.TabIndex = 8;
            this.labelKeyPIN.Text = "Key open PIN:";
            // 
            // textBoxKeyPIN
            // 
            this.textBoxKeyPIN.Location = new System.Drawing.Point(94, 80);
            this.textBoxKeyPIN.MaxLength = 8;
            this.textBoxKeyPIN.Name = "textBoxKeyPIN";
            this.textBoxKeyPIN.PasswordChar = '*';
            this.textBoxKeyPIN.Size = new System.Drawing.Size(76, 20);
            this.textBoxKeyPIN.TabIndex = 2;
            // 
            // labelPinHelp
            // 
            this.labelPinHelp.AutoSize = true;
            this.labelPinHelp.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.labelPinHelp.Location = new System.Drawing.Point(100, 103);
            this.labelPinHelp.Name = "labelPinHelp";
            this.labelPinHelp.Size = new System.Drawing.Size(61, 13);
            this.labelPinHelp.TabIndex = 9;
            this.labelPinHelp.Text = "8 digits PIN";
            // 
            // NewKeyForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(224, 169);
            this.Controls.Add(this.labelPinHelp);
            this.Controls.Add(this.labelKeyPIN);
            this.Controls.Add(this.textBoxKeyPIN);
            this.Controls.Add(this.labelPassword);
            this.Controls.Add(this.textBoxPassword);
            this.Controls.Add(this.labelEkey);
            this.Controls.Add(this.labelFileName);
            this.Controls.Add(this.buttonGenerate);
            this.Controls.Add(this.textBoxFileName);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "NewKeyForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Generate new key";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxFileName;
        private System.Windows.Forms.Button buttonGenerate;
        private System.Windows.Forms.Label labelFileName;
        private System.Windows.Forms.Label labelEkey;
        private System.Windows.Forms.TextBox textBoxPassword;
        private System.Windows.Forms.Label labelPassword;
        private System.Windows.Forms.Label labelKeyPIN;
        private System.Windows.Forms.TextBox textBoxKeyPIN;
        private System.Windows.Forms.Label labelPinHelp;
    }
}