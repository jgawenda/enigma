﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Enigma.Actions;
using Enigma.EncryptDecrypt;

namespace Enigma
{
    public partial class NewKeyForm : Form
    {
        private ValidatorNewKeyForm _validator;
        private SerializeOperator _serializeOperator;
        private RandomKeyGenerator _randomKeyGenerator;
        private TabStopOperator _tabStopOperator;

        public NewKeyForm()
        {
            InitializeComponent();
            _validator = new ValidatorNewKeyForm(new List<TextBox>() { textBoxFileName, textBoxPassword }, textBoxKeyPIN);
            _serializeOperator = new SerializeOperator();
            _randomKeyGenerator = new RandomKeyGenerator();
            _tabStopOperator = new TabStopOperator(new List<Control>() { labelEkey, labelFileName, labelKeyPIN, labelPassword, labelPinHelp });

            _tabStopOperator.Disable();
        }

        private void ButtonGenerate_Click(object sender, EventArgs e)
        {
            if (_validator.ValidateAllBoxes())
            {
                if (_serializeOperator.SaveNewEnigmaKey(textBoxFileName.Text, textBoxKeyPIN.Text, _randomKeyGenerator.Generate(textBoxPassword.Text)))
                    this.Close();
            }
        }

    }
}
