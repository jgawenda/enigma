﻿namespace Enigma
{
    partial class DecryptForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DecryptForm));
            this.labelNormalText = new System.Windows.Forms.Label();
            this.labelEncryptedText = new System.Windows.Forms.Label();
            this.labelKeyPassword = new System.Windows.Forms.Label();
            this.textBoxPassword = new System.Windows.Forms.TextBox();
            this.buttonDecrypt = new System.Windows.Forms.Button();
            this.labelLoadedKeyName = new System.Windows.Forms.Label();
            this.labelLoadedKey = new System.Windows.Forms.Label();
            this.richTextBoxAfter = new System.Windows.Forms.RichTextBox();
            this.richTextBoxBefore = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // labelNormalText
            // 
            this.labelNormalText.AutoSize = true;
            this.labelNormalText.Location = new System.Drawing.Point(384, 86);
            this.labelNormalText.Name = "labelNormalText";
            this.labelNormalText.Size = new System.Drawing.Size(63, 13);
            this.labelNormalText.TabIndex = 17;
            this.labelNormalText.Text = "Normal text:";
            // 
            // labelEncryptedText
            // 
            this.labelEncryptedText.AutoSize = true;
            this.labelEncryptedText.Location = new System.Drawing.Point(12, 86);
            this.labelEncryptedText.Name = "labelEncryptedText";
            this.labelEncryptedText.Size = new System.Drawing.Size(78, 13);
            this.labelEncryptedText.TabIndex = 16;
            this.labelEncryptedText.Text = "Encrypted text:";
            // 
            // labelKeyPassword
            // 
            this.labelKeyPassword.AutoSize = true;
            this.labelKeyPassword.Location = new System.Drawing.Point(12, 39);
            this.labelKeyPassword.Name = "labelKeyPassword";
            this.labelKeyPassword.Size = new System.Drawing.Size(76, 13);
            this.labelKeyPassword.TabIndex = 15;
            this.labelKeyPassword.Text = "Key password:";
            // 
            // textBoxPassword
            // 
            this.textBoxPassword.Location = new System.Drawing.Point(94, 36);
            this.textBoxPassword.Name = "textBoxPassword";
            this.textBoxPassword.PasswordChar = '*';
            this.textBoxPassword.Size = new System.Drawing.Size(100, 20);
            this.textBoxPassword.TabIndex = 0;
            // 
            // buttonDecrypt
            // 
            this.buttonDecrypt.Location = new System.Drawing.Point(306, 206);
            this.buttonDecrypt.Name = "buttonDecrypt";
            this.buttonDecrypt.Size = new System.Drawing.Size(75, 23);
            this.buttonDecrypt.TabIndex = 2;
            this.buttonDecrypt.Text = "Decrypt";
            this.buttonDecrypt.UseVisualStyleBackColor = true;
            this.buttonDecrypt.Click += new System.EventHandler(this.ButtonDecrypt_Click);
            // 
            // labelLoadedKeyName
            // 
            this.labelLoadedKeyName.AutoSize = true;
            this.labelLoadedKeyName.Location = new System.Drawing.Point(85, 9);
            this.labelLoadedKeyName.Name = "labelLoadedKeyName";
            this.labelLoadedKeyName.Size = new System.Drawing.Size(31, 13);
            this.labelLoadedKeyName.TabIndex = 12;
            this.labelLoadedKeyName.Text = "none";
            // 
            // labelLoadedKey
            // 
            this.labelLoadedKey.AutoSize = true;
            this.labelLoadedKey.Location = new System.Drawing.Point(12, 9);
            this.labelLoadedKey.Name = "labelLoadedKey";
            this.labelLoadedKey.Size = new System.Drawing.Size(67, 13);
            this.labelLoadedKey.TabIndex = 11;
            this.labelLoadedKey.Text = "Loaded Key:";
            // 
            // richTextBoxAfter
            // 
            this.richTextBoxAfter.Location = new System.Drawing.Point(387, 102);
            this.richTextBoxAfter.Name = "richTextBoxAfter";
            this.richTextBoxAfter.Size = new System.Drawing.Size(287, 242);
            this.richTextBoxAfter.TabIndex = 3;
            this.richTextBoxAfter.Text = "";
            // 
            // richTextBoxBefore
            // 
            this.richTextBoxBefore.Location = new System.Drawing.Point(13, 102);
            this.richTextBoxBefore.Name = "richTextBoxBefore";
            this.richTextBoxBefore.Size = new System.Drawing.Size(287, 242);
            this.richTextBoxBefore.TabIndex = 1;
            this.richTextBoxBefore.Text = "";
            // 
            // DecryptForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(686, 358);
            this.Controls.Add(this.labelNormalText);
            this.Controls.Add(this.labelEncryptedText);
            this.Controls.Add(this.labelKeyPassword);
            this.Controls.Add(this.textBoxPassword);
            this.Controls.Add(this.buttonDecrypt);
            this.Controls.Add(this.labelLoadedKeyName);
            this.Controls.Add(this.labelLoadedKey);
            this.Controls.Add(this.richTextBoxAfter);
            this.Controls.Add(this.richTextBoxBefore);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "DecryptForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Decrypt message";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelNormalText;
        private System.Windows.Forms.Label labelEncryptedText;
        private System.Windows.Forms.Label labelKeyPassword;
        private System.Windows.Forms.TextBox textBoxPassword;
        private System.Windows.Forms.Button buttonDecrypt;
        private System.Windows.Forms.Label labelLoadedKeyName;
        private System.Windows.Forms.Label labelLoadedKey;
        private System.Windows.Forms.RichTextBox richTextBoxAfter;
        private System.Windows.Forms.RichTextBox richTextBoxBefore;
    }
}